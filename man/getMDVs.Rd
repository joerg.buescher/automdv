% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/getMDVs.R
\name{getMDVs}
\alias{getMDVs}
\title{This function processes extracts peak intensities for all relevant isotopologues and returns them as peak areas and as normalized MDVs}
\usage{
getMDVs(filelist, metab, RTshift, prm)
}
\arguments{
\item{metab}{data structure with information on target metabolites}

\item{RTshift}{numeric vector detailing the shift in RT between information given in metab and samples}

\item{prm}{parameters for processing}

\item{filelit}{list of mzML file names}
}
\value{
report
}
\description{
This function processes extracts peak intensities for all relevant isotopologues and returns them as peak areas and as normalized MDVs
}
