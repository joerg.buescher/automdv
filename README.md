# autoMDV

Fully automatic processing of GC-MS data for analysis of 13C labelling experiments

## What this is about?

This simple R package is designed for the automatic and targeted analysis of 13C labelling experiments measured by GC-MS. It used established ideas from the scientific literature and allows you to apply them to your data hassle free. For more background please read the following publications:

- (1) Antoniewicz, M. R.; Kelleher, J. K.; Stephanopoulos, G. Accurate Assessment of Amino Acid Mass Isotopomer Distributions for Metabolic Flux Analysis. Anal. Chem. 2007, 79 (19), 7554–7559. https://doi.org/10.1021/ac0708893.
- (2) Buescher, J. M.; Antoniewicz, M. R.; Boros, L. G.; Burgess, S. C.; Brunengraber, H.; Clish, C. B.; DeBerardinis, R. J.; Feron, O.; Frezza, C.; Ghesquière, B.; Gottlieb, E.; Hiller, K.; Jones, R. G.; Kamphorst, J. J.; Kibbey, R. G.; Kimmelman, A. C.; Locasale, J. W.; Lunt, S. Y.; Maddocks, O. D.; Malloy, C.; Metallo, C. M.; Meuillet, E. J.; Munger, J.; Nöh, K.; Rabinowitz, J. D.; Ralser, M.; Sauer, U.; Stephanopoulos, G.; St-Pierre, J.; Tennant, D. A.; Wittmann, C.; Vander Heiden, M. G.; Vazquez, A.; Vousden, K.; Young, J. D.; Zamboni, N.; Fendt, S. A Roadmap for Interpreting 13C Metabolite Labeling Patterns from Cells. Curr. Opin. Biotechnol. 2015, 34, 189–201. https://doi.org/10.1016/j.copbio.2015.02.003.

## What you'll need

- [ ] In a folder on your hard drive (the project folder) you will need multiple (I've never tested with less than 3) files in .mzML format holding the raw data from your GC-MS measurements in centroid mode.
- [ ] In addition, the project folder must contain a .csv file that defines your processing parameters. Have a look at initPrm.R for the parameters that can be set and their default values. Probably you will want at least set the location where your metabolite definition file can be found. An example file is included in the repository (update_prm.csv). DO NOT rename this file.
- [ ] Somewhere on your hard drive you will need a .xlsx file holding the metabolite definitions. The values in this depend on your GCMS method. An example file is included in the repository (example_metab.xlsx). You can rename this file in any way you like, just make sure that the update_prm.csv maches.

## Processing your first data set

Once you have autoMDV installed and you have a folder with your .mzML files and the update_prm.csv file detailing the location of your metabolite definitions, you can start the processing in R with this command:

autoMDV::processBatch('path/to/project/folder/')

In the R console you should then be able to follow the progress of the processing. Once it is finished, you should find 3 additional files in your project folder:
- R_messages.log : a log file mainly good for troubleshooting
- peakinfo.xlsx : The result you were looking for
- peakoverview.pdf : A visual representation of all peaks that were analyzed. NOT meant for printing.
 
