#' print overview of EICs to pdf file
#'
#' @param filelit list of mzML file names
#' @param metab data structure with information on target metabolites
#' @param report output of getMDV, maybe filtered by detectFrequencyFilter
#' @param prm parameters for processing
#' @param smpl additional sample information
#'
#' @export
plotPeakOverview <- function(filelist, metab, report, smpl, prm) {
  
  # create peak review plots
  pdf(file = file.path(prm$batchdir, "peakoverview.pdf"), height = 2*length(filelist), width = 3* length(metab),  family = "Helvetica")
  layout(matrix(c(1:(length(metab)*length(filelist))), length(filelist), length(metab), byrow = TRUE)) # initiate subplots
  
  plotinfo <- report$plotinfo
  
  for (id in order(smpl$sorttype[1:length(filelist)]) ){
    for (igm in 1:length(metab)){
      plotlabel <- paste0(metab[[igm]]$name, ' in ', filelist[id], '\n',
                          report$type[id], '\n',
                          'M0 = ' , metab[[igm]]$isotopelist[1] , ' m/z') 
      
      nowistart <- nowiend <- metab[[igm]]$rt*60
      intstart <- 1
      intend <- length(plotinfo[[igm]][[id]]$plottrace)
      if (!is.null(plotinfo[[igm]][[id]]$intstart)) {
        nowistart <- plotinfo[[igm]][[id]]$rt[plotinfo[[igm]][[id]]$intstart]/60
        nowiend   <- plotinfo[[igm]][[id]]$rt[plotinfo[[igm]][[id]]$intend]/60
        intstart <- plotinfo[[igm]][[id]]$intstart
        intend <- plotinfo[[igm]][[id]]$intend
      }
      
      plotmax <- 100
      for (ip in 1:length(plotinfo[[igm]][[id]]$plottrace)) {
        plotmax <- max(plotmax, plotinfo[[igm]][[id]]$plottrace[[ip]][intstart:intend], na.rm = TRUE)
      }

      if (is.null(plotinfo[[igm]][[id]]$rt)) {
        plot(0,0)
        next
      } 
      
      plot(0, 0, xlim = range(plotinfo[[igm]][[id]]$rt)/60, ylim=c(0,plotmax), xlab='', ylab='', cex.main=0.7, main= plotlabel)
      
      # indicate integrated range
      polygon(x = c(nowistart, nowistart, nowiend,    nowiend),
              y = c(-plotmax,  2*plotmax, 2*plotmax, -plotmax),
              border = NA, col = '#33FFAA'
      )
      
      lines(plotinfo[[igm]][[id]]$rt/60, plotinfo[[igm]][[id]]$plottrace[[1]], col = 'red')
      
      isonum <- metab[[igm]]$maxlabel +1 #length(plotinfo[[igm]][[id]]$plottrace)-1
      for (im in 1:isonum ) {
        nowcol <- paste0('#', paste( rep(as.character(as.hexmode(271-round(55 + (isonum+1-im)*(200/isonum)))),3), collapse = ''))
        lines(plotinfo[[igm]][[id]]$rt/60, plotinfo[[igm]][[id]]$plottrace[[im+1]] , col = nowcol)
      } # endofor all isotopologues
      
      # add expected RT
      lines(rep(metab[[igm]]$rt ,2),
            c(0,-100000),
            lwd=3, col="red")
      
    } # endfor all mets
  } # endfor all files
  dev.off()
  
  
}