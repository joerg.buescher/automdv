#' initialize default parameter list
#'
#' This function initialized the list of required parameters to default values.
#'
#' @return prm
#'
#' @export 
#' 
prmInit <- function() {
  
  prm <- list()
  
  prm$reportall <- FALSE # ignore low intensity filter
  prm$runninglocal <- TRUE # to activate debug loops
  prm$compoundinfoxlsx <-  '~/mswebdata/helperfiles/metabolite_databases/compoundinfo_TBDMS.xlsx'
  # compoundinfoxlsx <- paste0(pathprefix, 'data/helperfiles/metabolite_databases/compoundinfo13C_TMS.xlsx')
  # compoundinfoxlsx <- paste0(pathprefix, 'data/helperfiles/metabolite_databases/compoundinfo_FA.xlsx')
  
  # GC-method dependent settings TBDMS labeling
  prm$peakwidthlimit <- c(2,40) # minimum and maximum peak width in seconds
  prm$samplingfrequency <- 5 # samplingfrequency (time resolution of analysis) in Hz the smaller the number, the faster the processing. BUT low sampling frequencies risk messing up the data.
  # 5 Hz sampling frequency roughly matches the acually recorded sampling frequency
  # mzrange <- c(100,650) # min and max of measured range in m/z
  prm$timerange <- c(0,60) # c(7.1,88.5) # time range in minutes
  # other settings
  # prm$intensitymeasure <- 'intb' # intensity above 0. Alternatives: gaussfit, into, intb and maxo, the area and the hight above zero. but doublecheck the code, because not all parts might be available for peak based intensity measures
  prm$smoothnum <- 3 # number of points to use in rollmean smoothing
  prm$minheight <- 10 # minimum value of maxo to include a peak
  prm$minarea <- 1000 # minimum peak area for reporting
  
  prm$hardRTcutoff <- 20 # not consider peaks outside this window. omit this filter when working with data from different GC methods.
  prm$masstol <- 0.3 # mass tolerance  for getting EICs
  prm$extraisotopes <- 3 # number of additional isotopes beyond maxlabel that will be fitted 
  prm$mingoodfraction <- 0.15 # fraction of samples that must give useful data for a metabolite in order to report the metabolite
  prm$halfshifttreshold <- 12*60 # below this time in sec half the RT shift is applied
  prm$correlpenalty <- 0.4 # minimum correlation 
  prm$ratiopenalty <- 0.1 # minimum ratio score
  
  prm$istdWindow <- 60 # Allowed RT distance between measured peak and reference list for peaks selected for RT alignment
  prm$minIstdIntens <- 50000 # minimum peak intensity for reference peaks for RT alignment

  prm$fancyformat <- FALSE # add beautiful formatting to xlsx output
  
  prm
}